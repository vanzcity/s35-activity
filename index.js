const express = require("express")
const mongoose = require("mongoose")
require('dotenv').config() //Initialize ENV file
const app = express()
const port = 3000

// Mongoose connection
// You can use an ENV file to hide the password from the code itself. You can also put the database name before the '?' in the connection string.
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@cluster0.ox4nlw1.mongodb.net/S35-DISCUSSION?retryWrites=true&w=majority`, {
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

// The 'db' variable represents the connection which is established after running the 'connect()' function
let db = mongoose.connection 

// Listening for events in the connection. The "error" event will trigger only if the connection has an error. And the "open" event will trigger only if it has successfully connected to MongoDB.
db.on("error", () => console.log("Connection error"))
db.once("open", () => console.log("Connected to MongoDB!"))

// [SECTION] Schemas
// Represents the STRUCTURE of a table/collection
const user_schema = new mongoose.Schema({
	username: String,
	password: String
})

// [SECTION] Models
// Is the representation of the 'tasks' table. Models always follow the naming convention where the first letter of the table is ALWAYS capital letter and the word itself is singular.
const Active = mongoose.model("Active", user_schema)

// Middleware registration
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// [SECTION] Routes
// Create new task
app.post('/signup', (request, response) => {
	// findOne() is a method that acts like a query for the Task model ('tasks' collection). It finds a specific item based on the parameters that you put in it.
	Active.findOne({username: request.body.username, password: request.body.password}).then((result, error) => {
		// Checks if the task name is already existing in the database
		if(result != null && result.username === request.body.username && result.password === request.body.password){
			return response.send("Duplicate user found!")
		} else { // If there are no duplicates, proceed with the creation of the task

			// 1. Create a new instance of the 'Task' model containing properties required based on the schema of that model
			let new_user = new Active({
				username: request.body.username,
				password: request.body.password
			})

			// 2. Save that new instance of the 'Task' model into the database and handle any errors accordingly
			new_user.save().then((added_user, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New user added!")
				}
			})
		}
	})
})

// Get all tasks
app.get("/signup", (request, response) => {
		Active.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		} 

		return response.status(200).send(result)
	})
})

app.listen(port, () => console.log(`The server is running at localhost:${port}`))
